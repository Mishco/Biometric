﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometric
{
    public interface MySensors
    {
        string GetToast();
        string GetSensorsList();
    }
}

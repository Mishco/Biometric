﻿using System;
using Xamarin.Forms;
using Plugin.DeviceInfo;
using DeviceMotion;
using DeviceMotion.Plugin;
using DeviceMotion.Plugin.Abstractions;
using System.Diagnostics;

namespace Biometric
{
	public partial class MainPage : ContentPage
	{
		public MainPage ()
		{
			InitializeComponent ();
		}

		async void OnLogoutButtonClicked (object sender, EventArgs e)
		{
			App.IsUserLoggedIn = false;
			Navigation.InsertPageBefore (new LoginPage (), this);
			await Navigation.PopAsync ();
		}
        /// <summary>
        /// Starting button for login user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //async void OnStartClick (object sender, EventArgs e)
        //{


        //    //try
        //    //{
        //    //    labelVersion.Text = "Version: " +CrossDeviceInfo.Current.Version;
        //    //    labelPlatform.Text = "Platform: " + CrossDeviceInfo.Current.Platform;
        //    //    labelModel.Text = "Model: " + CrossDeviceInfo.Current.Model;
        //    //    labelId.Text = "Id: " + CrossDeviceInfo.Current.Id;
        //    //} catch (Exception ex)
        //    //{
        //    //    Debug.WriteLine("ERR>  " + ex.Message);
        //    //}
            

        //    //CrossDeviceMotion.Current.SensorValueChanged += (s, a) => {

        //    //    switch (a.SensorType)
        //    //    {
        //    //        case MotionSensorType.Accelerometer:
        //    //            Debug.WriteLine("A: {0},{1},{2}", ((MotionVector)a.Value).X, ((MotionVector)a.Value).Y, ((MotionVector)a.Value).Z);
        //    //            break;
        //    //        case MotionSensorType.Compass:
        //    //            Debug.WriteLine("H: {0}", a.Value);
        //    //            break;
        //    //    }
        //    //};

        //    //CrossDeviceMotion.Current.Start(MotionSensorType.Accelerometer, MotionSensorDelay.Ui);
        //    //CrossDeviceMotion.Current.SensorValueChanged += (s, a) =>
        //    //{
        //    //    switch (a.SensorType)
        //    //    {
        //    //        case MotionSensorType.Accelerometer:

        //    //            lblXAxis.Text = ((MotionVector)a.Value).X.ToString("F");
        //    //            lblYAxis.Text = ((MotionVector)a.Value).Y.ToString("F");
        //    //            lblZAxis.Text = ((MotionVector)a.Value).Z.ToString("F");
        //    //            break;
        //    //    }
        //    //};


        //}
        //async void OnEndClick(object sender, EventArgs e)
        //{

        //}

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using DeviceMotion.Plugin;
using DeviceMotion.Plugin.Abstractions;
using System.Diagnostics;
using Biometric.Logic;

namespace Biometric.Views
{
	public partial class TestSensors : ContentPage
	{
        public TestSensors()
        {
            InitializeComponent();

            InfoAboutDevice inf = new InfoAboutDevice();
            inf.InfoAboutSensors();

            if (CrossDeviceMotion.Current.IsActive(MotionSensorType.Accelerometer))
            {
                lblAkc.TextColor = Color.Green;
            } else
            {
                lblAkc.TextColor = Color.Red;
            }


        }
	}
}
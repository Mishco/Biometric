﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Biometric.Model;

namespace Biometric.Views
{
	public partial class MasterPage : ContentPage
	{
        public ListView ListView { get { return listView; } }

        public MasterPage ()
		{
			InitializeComponent ();

            var masterPageItems = new List<MasterPageItem>();
            masterPageItems.Add(new MasterPageItem
            {
                Title = "About",
                IconSource = "contacts.png",
                TargetType = typeof(AboutPage)
            });
            masterPageItems.Add(new MasterPageItem
            {
                Title = "Tests sensors",
                IconSource = "todo.png",
                TargetType = typeof(TestSensors)
            });
            masterPageItems.Add(new MasterPageItem
            {
                Title = "Kreslenie",
                IconSource = "reminders.png",
                TargetType = typeof(PaintPage)
            });

            listView.ItemsSource = masterPageItems;
        }
	}
}
﻿using System;
using System.Diagnostics;

using Xamarin.Forms;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System.Collections.Generic;
using TouchTracking;
using System.Text;
using System.IO;
using PCLStorage;
using Biometric.Logic;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Linq;
using Rg.Plugins.Popup.Extensions;

namespace Biometric.Views
{
    public partial class PaintPage : ContentPage
    {
        bool showFill = true;
        private String baseUrl = "http://147.175.149.228";
        Dictionary<long, SKPath> inProgressPaths = new Dictionary<long, SKPath>();
        List<SKPath> completedPaths = new List<SKPath>();
        byte[] buffer = null;
        byte[] buffer_tmp = null;
        private static IFile file = null;

        private User _user;
        private int _countOftest;
        private int _userId;


        // Button Click
        private async void OnOpenPupup(object sender, EventArgs e)
        {
            try
            {
                var page = new TextPage(); 
                await Navigation.PushPopupAsync(page);
                // or
                // await PopupNavigation.PushAsync(page);
            }catch(Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }




        public PaintPage(User user, int countOfTtest)
        {
            InitializeComponent();

            _user = user;
            _countOftest = 22;
            _userId = -1;
            buffer = Encoding.UTF8.GetBytes("");    // init buffer

            RegisterUserAsync(user);

            var o = DisplayAlert("Registracia", "Registracia pozostava z viacerych krokov, prvym krokom bude kreslenie", "ok");

            //var page = new TextPage();
            //Navigation.PushPopupAsync(page);
        }

        public static String GetTimestamp(DateTime value)
        {
            ////Find unix timestamp (seconds since 01/01/1970)
            //long ticks = DateTime.UtcNow.Ticks - DateTime.Parse("01/01/1970 00:00:00").Ticks;
            //ticks /= 10000000; //Convert windows ticks to seconds
            //String timestamp = ticks.ToString();
            //return timestamp;

            /// Old version
            return value.ToString("yyyyMMddHHmmssffff");
        }

        SKPaint paint = new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            Color = SKColors.Red,
            StrokeWidth = 10,
            StrokeCap = SKStrokeCap.Round,
            StrokeJoin = SKStrokeJoin.Round
        };

        void OnTouchEffectAction(object sender, TouchActionEventArgs args)
        {
            try
            {


                switch (args.Type)
                {
                    case TouchActionType.Pressed:
                        if (!inProgressPaths.ContainsKey(args.Id))
                        {
                            SKPath path = new SKPath();
                            path.MoveTo(ConvertToPixel(args.Location));
                            inProgressPaths.Add(args.Id, path);
                            canvasView.InvalidateSurface();

                            //Debug.WriteLine("Pressed " + GetTimestamp(DateTime.Now) + " X: " + args.Location.X + " Y: " + args.Location.Y);
                            buffer = Encoding.UTF8.GetBytes("Pressed;" + GetTimestamp(DateTime.Now) + ";" + args.Location.X + ";" + args.Location.Y + "\n");
                            //var o = DisplayAlert("Pressed", "press " + args.Location, "ok");
                        }
                        break;

                    case TouchActionType.Moved:
                        if (inProgressPaths.ContainsKey(args.Id))
                        {
                            SKPath path = inProgressPaths[args.Id];
                            path.LineTo(ConvertToPixel(args.Location));
                            canvasView.InvalidateSurface();
                            //Debug.WriteLine("Move " + GetTimestamp(DateTime.Now) + " X: " + args.Location.X + " Y: " + args.Location.Y);
                            buffer_tmp = Encoding.UTF8.GetBytes("Move;" + GetTimestamp(DateTime.Now) + ";" + args.Location.X + ";" + args.Location.Y + "\n");
                            //Buffer.BlockCopy(buffer_tmp, 0, buffer, 0, buffer_tmp.Length);
                            buffer = buffer.Concat(buffer_tmp).ToArray();

                            //buffer_tmp = null;
                        }
                        break;

                    case TouchActionType.Released:
                        if (inProgressPaths.ContainsKey(args.Id))
                        {
                            completedPaths.Add(inProgressPaths[args.Id]);
                            inProgressPaths.Remove(args.Id);
                            canvasView.InvalidateSurface();
                            //Debug.WriteLine("Release " + GetTimestamp(DateTime.Now) + " X: " + args.Location.X + " Y: " + args.Location.Y);
                            buffer_tmp = Encoding.UTF8.GetBytes("Release;" + GetTimestamp(DateTime.Now) + ";" + args.Location.X + ";" + args.Location.Y + "\n");
                            //Buffer.BlockCopy(buffer_tmp, 0, buffer, 0, buffer_tmp.Length);
                            buffer = buffer.Concat(buffer_tmp).ToArray();

                            WriteBuffersIntoFile(buffer, file); // append buffer to file

                            buffer_tmp = null;


                        }
                        break;

                    case TouchActionType.Cancelled:
                        if (inProgressPaths.ContainsKey(args.Id))
                        {
                            inProgressPaths.Remove(args.Id);
                            canvasView.InvalidateSurface();


                            // OnNextButtonCliked

                        }
                        break;
                }
            }catch(Exception e)
            {
                Debug.WriteLine(e);
                var o = DisplayAlert("Chyba", "Chyba " + e, "ok");
            }
        }
        async void OnNextButtonClicked(object sender, EventArgs e)
        {
            
            await SendDataToServerAsync(file);

            // continue to registration
            var registrationPage = Navigation.NavigationStack.FirstOrDefault();
            if (registrationPage != null)
            {
                Navigation.InsertPageBefore(new Registration(_user, _countOftest), Navigation.NavigationStack.First());
                await Navigation.PopToRootAsync();
            }
        }

        async void WriteBuffersIntoFile(byte[] buff, IFile file)
        {
            if (file != null)
            {
                if (buff != null)
                {
                    Stream streamToWrite = null;
                    try
                    {
                        using (streamToWrite = await file.OpenAsync(FileAccess.ReadAndWrite).ConfigureAwait(false))
                        {
                            streamToWrite.Position = streamToWrite.Length;

                            if (streamToWrite.CanWrite)
                            {
                                await streamToWrite.WriteAsync(buff, 0, buff.Length).ConfigureAwait(false);
                            }
                        }
                    }
                    catch (Exception exe)
                    {
                        Debug.WriteLine("ERR> Paint buffer to csv: " + exe);
                    }
                    finally
                    {
                        if (streamToWrite != null)
                        {
                            streamToWrite.Dispose();
                        }
                    }
                }
            }
        }

        public async Task SendDataToServerAsync(IFile file)
        {
            var RestUrl = baseUrl + "/mslovik/upload.php";
            var uri = new Uri(string.Format(RestUrl, string.Empty));


            if (file == null)
            {
                Debug.WriteLine("Error: Empty file");
                return;
            }
            // Read content from file
            var x = await file.ReadAllTextAsync();
            HttpClient httpClient = null;
            try
            {
                using (httpClient = new HttpClient())
                {
                    var surveyBytes = Encoding.UTF8.GetBytes("Type;Time;X;Y\n" + x);

                    // Add headers for upload 
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var byteArrayContent = new ByteArrayContent(surveyBytes);
                    byteArrayContent.Headers.ContentType = MediaTypeHeaderValue.Parse("text/csv");

                    // Http Post with data file
                    var response = await httpClient.PostAsync(uri, new MultipartFormDataContent
                    {
                        {byteArrayContent, "\"file\"", file.Name}
                    });

                    if (response.IsSuccessStatusCode)
                    {
                        Debug.WriteLine(response.StatusCode);
                    }
                    else
                    {
                        Debug.WriteLine("Error while uploading file on web server" + response);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            finally
            {
                if (httpClient != null)
                {
                    httpClient.Dispose();
                }
            }
        }// end SendDataToServerAsync()
    

        void OnCanvasViewPaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {
            try
            {

                SKCanvas canvas = args.Surface.Canvas;
                canvas.Clear();

                foreach (SKPath path in completedPaths)
                {
                    canvas.DrawPath(path, paint);
                }

                foreach (SKPath path in inProgressPaths.Values)
                {
                    canvas.DrawPath(path, paint);
                }
            }catch(Exception e)
            {

                var o = DisplayAlert("Chyba", "Chyba " + e, "ok");
            }
        }

        SKPoint ConvertToPixel(Point pt)
        {
            return new SKPoint((float)(canvasView.CanvasSize.Width * pt.X / canvasView.Width),
                               (float)(canvasView.CanvasSize.Height * pt.Y / canvasView.Height));
        }

        async void CreateFileAsync()
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFolder folder = await rootFolder.CreateFolderAsync("DataFolder", CreationCollisionOption.OpenIfExists);
            String nameOffile = _userId + "_" + (DateTime.Now.Ticks / 1000) + "_paint_data_" + new InfoAboutDevice().Model + ".csv";

            // Global value
            file = await folder.CreateFileAsync(nameOffile, CreationCollisionOption.ReplaceExisting);
        }

        public async void RegisterUserAsync(User user)
        {
            String tmpEmail = user.Email;
            String tmpPass = user.Password;

            HttpClient client = null;
            try
            {
                using (client = new HttpClient())
                {

                    client.BaseAddress = new Uri(baseUrl);

                    var content = new FormUrlEncodedContent(new[]
                    {
                         new KeyValuePair<string, string>("email", tmpEmail),
                         new KeyValuePair<string, string>("pass", tmpPass)
                    });
                    // Create HTTP Post to webServer and register new user into database
                    var result = await client.PostAsync("/mslovik/registerNewUser.php", content);

                    // Wait for response
                    string resultContent = await result.Content.ReadAsStringAsync();

                    if (result.IsSuccessStatusCode)
                    {
                        Debug.WriteLine("OK_RESULT: ");
                        Debug.WriteLine(resultContent);
                        Char delimiter = ':';
                        String[] substrings = resultContent.Split(delimiter);
                        _userId = Int32.Parse(substrings[0]);
                        Debug.WriteLine("User id from phpMyAdmin: " + _userId);
                        _user.UserID = _userId;

                        // Create a file 
                        CreateFileAsync();
                    }
                    else
                    {
                        Debug.WriteLine("Register already done!");
                        Debug.WriteLine(result);
                        Char delimiter = ':';
                        String[] substrings = resultContent.Split(delimiter);
                        _userId = Int32.Parse(substrings[0]);
                        _user.UserID = _userId;
                    }
                }
            }
            catch (Exception ex)
            {
                var obj3 = DisplayAlert("ERR: Registration", ex.ToString(), "ok");
                Debug.WriteLine("ERR:" + ex);
            }
            finally
            {
                if (client != null)
                {
                    client.Dispose();
                }
            }
        }
    }
}
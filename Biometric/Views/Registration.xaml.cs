﻿using DeviceMotion.Plugin;
using DeviceMotion.Plugin.Abstractions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using PCLStorage;
using System.Net.Http;
using System.Net.Http.Headers;
using Plugin.Connectivity;
using Biometric.Logic;


namespace Biometric
{
    public partial class Registration : ContentPage
    {
        private String baseUrl = "http://147.175.149.228";
        private static IFile file;
        public int countOfTtest;
        public static int _userId = -1; // admin@gmail.com
        //private bool firstTime = true;
        private List<IFile> list_of_files;
        static object _locker = new object();
        SensorsCatching s = null;
        //TapGestureRecognizer tapGestureRecognizer = null;
        public static IFile File => file;

        /// <summary>
        /// Default empty constructor
        /// </summary>
        public Registration()
        {
            InitializeComponent();
            //CreateFile();
        }
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="user">New user from SignUpPage.xaml</param>
        public Registration(User user, int countOfTtestFromPaint)
        {
            InitializeComponent();
            
            countOfTtest = countOfTtestFromPaint;
            Debug.WriteLine("DEBUG: " + countOfTtestFromPaint + " , " + countOfTtest);
            textTests.Text = (countOfTtest+1).ToString();
            list_of_files = new List<IFile>();

            if (CrossConnectivity.Current.IsConnected)
            {
                if(user.UserID > 0)
                {
                    // user ID was getting before   
                    _userId = user.UserID;
                    btnStart.IsEnabled = true;
                } else
                {
                    Debug.WriteLine("Start registration");
                    RegisterUserAsync(user);
                }
            }
            else
            {
                Debug.WriteLine("No Internet is available ");
                var object2 = DisplayAlert("Error Connection", "No Internet is available", "ok");
            }

            InfoAboutDevice info = new InfoAboutDevice();
            Debug.WriteLine(info.Platform + "\n" + info.Version + " " + info.VersionNumber + " " + info.Model + " " + info.Id);
            labelPlatform.Text = info.Platform + "\n" +  info.Version + "\n" + info.Model;

            // Initialize tapped gesture
            //tapGestureRecognizer = new TapGestureRecognizer();
            //tapGestureRecognizer.Tapped += (s, ex) =>
            //{
            //    // Handle the tap
            //    Debug.WriteLine("Clicked");
            //    OnEndClick(s, ex);
            //};
        }

        /// <summary>
        /// Register new user into web server
        /// </summary>
        /// <param name="user">new user from SignUpPage</param>
        /// <returns>Return id of new register user from remote database</returns>
        public async void RegisterUserAsync(User user)
        {
            String tmpEmail = user.Email;
            String tmpPass = user.Password;

            HttpClient client = null;
            try
            {
                using (client = new HttpClient())
                {
                   
                    client.BaseAddress = new Uri(baseUrl);
                    
                    var content = new FormUrlEncodedContent(new[]
                    {
                         new KeyValuePair<string, string>("email", tmpEmail),
                         new KeyValuePair<string, string>("pass", tmpPass)
                    });
                    // Create HTTP Post to webServer and register new user into database
                    var result = await client.PostAsync("/mslovik/registerNewUser.php", content);

                    // Wait for response
                    string resultContent = await result.Content.ReadAsStringAsync();

                    if (result.IsSuccessStatusCode)
                    {
                        Debug.WriteLine("OK_RESULT: ");
                        Debug.WriteLine(resultContent);
                        Char delimiter = ':';
                        String[] substrings = resultContent.Split(delimiter);
                        _userId = Int32.Parse(substrings[0]);
                        Debug.WriteLine("User id from phpMyAdmin: " + _userId);
                        btnStart.IsEnabled = true;
                    }
                    else
                    {
                        Debug.WriteLine("Register already done!");
                        Debug.WriteLine(result);
                        Char delimiter = ':';
                        String[] substrings = resultContent.Split(delimiter);
                        _userId = Int32.Parse(substrings[0]);
                    }
                }
            }
            catch (Exception ex)
            {
                var obj3 = DisplayAlert("RegistrationERROR: ", ex.ToString(), "ok");
                Debug.WriteLine("ERR:" + ex);
            }
            finally
            {
                if (client!= null)
                {
                    client.Dispose();
                }
            }
        }
        /// <summary>
        /// Create new temporary file .csv type
        /// File consist of userID_timestamp_data
        /// Every files are saved into mobile in directory "DataFolder"
        /// </summary>
        async Task<IFile> CreateFile()
        {
            //CreateDataFile cd = new CreateDataFile(_userId);
            //cd.DoCreateDataFile(countOfTtest);
            //file = CreateDataFile._File;
            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFolder folder = await rootFolder.CreateFolderAsync("DataFolder", CreationCollisionOption.OpenIfExists);
            String nameOffile = _userId + "_" + (DateTime.Now.Ticks / 1000) + "_" + countOfTtest + "_data_" + new InfoAboutDevice().Model + ".csv";

            file = await folder.CreateFileAsync(nameOffile, CreationCollisionOption.ReplaceExisting);
            return file;
        }


        /// <summary>
        /// Method to start registration
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnStartClick(object sender, EventArgs e)
        {
            // If user does not exists
            if (_userId != -1)
            {
                var filef = CreateFile();
                s = new SensorsCatching();
                s.Run();

                //labelCotrol.TextColor = Color.Red;
                //labelCotrol.Text = "Running";

                btnStop.IsEnabled = true;
                StackLayoutMain.BackgroundColor = Color.Green;
                //StackLayoutMain.GestureRecognizers.Add(tapGestureRecognizer);
            }
            else
            {
                var object2 = DisplayAlert("Error Registration", "Register was not succesfull. Please repeat it.", "ok");
                return;
            }
        }

        //async void WriteBuffersIntoFile(byte[] buffAcc, byte[] buffComp, byte[] buffGyro, byte[] buffMagn, IFile file)
        //{
        //    Stream streamToWrite;
        //    byte[] bytes = Encoding.UTF8.GetBytes("");
        //    if (file != null)
        //    {
        //        if (buffAcc != null && buffComp != null && buffGyro != null && buffMagn != null)
        //        {
        //            try
        //            {
        //                // Write buffers into file 
        //                using (streamToWrite = await file.OpenAsync(FileAccess.ReadAndWrite).ConfigureAwait(false))
        //                {
        //                    streamToWrite.Position = streamToWrite.Length;
        //                    if (streamToWrite.CanWrite)
        //                    {
        //                            // Concat buffers
        //                            bytes = buffAcc.Concat(buffComp).Concat(buffMagn).Concat(buffGyro).ToArray();
        //                            await streamToWrite.WriteAsync(bytes, 0, bytes.Length).ConfigureAwait(false);
        //                    }
        //                }
        //            }
        //            catch (Exception exe)
        //            {
        //                // TODO checking open writing csv
        //                // System.IO.IOException: Sharing violation on path /data/data/com.mishco.Biometric/files/DataFolder/3_636433899487625_4_data_GT-I9195.csv
        //                //Debug.WriteLine("Writing csv: " + exe);
        //            }
        //        }
        //    }

        //}

        //byte[] StringToByte(String text)
        //{
        //    return Encoding.UTF8.GetBytes(text);
        //}


        //async void GetSensorsStream()
        //{
        //    // For every sensor individually byte buffer 
        //    var bufferArrayAccel = Encoding.UTF8.GetBytes("");
        //    var bufferArrayComp = Encoding.UTF8.GetBytes("");
        //    var bufferArrayGyro = Encoding.UTF8.GetBytes("");
        //    var bufferArrayMagne = Encoding.UTF8.GetBytes("");

        //    try
        //    {
        //            CrossDeviceMotion.Current.Start(MotionSensorType.Accelerometer, MotionSensorDelay.Fastest);
        //            CrossDeviceMotion.Current.Start(MotionSensorType.Compass, MotionSensorDelay.Fastest);
        //            CrossDeviceMotion.Current.Start(MotionSensorType.Gyroscope, MotionSensorDelay.Fastest);
        //            CrossDeviceMotion.Current.Start(MotionSensorType.Magnetometer, MotionSensorDelay.Fastest);

        //        // If sensors catch same value write them
        //        CrossDeviceMotion.Current.SensorValueChanged += async (s, a) =>
        //        {
        //            int lastLog = Environment.TickCount;
        //            if (Environment.TickCount > lastLog-1)
        //            {
        //                switch (a.SensorType)
        //                {
        //                    case MotionSensorType.Accelerometer:
        //                        /*
        //                         * 
        //                         * //Debug.WriteLine(new Vector3(((MotionVector)a.Value).X, ((MotionVector)a.Value).Y, ((MotionVector)a.Value).Z) / gravity);
        //                         * // float gravity = Android.Hardware.SensorManager.GravityEarth;
        //                         * // https://github.com/labnation/MonoGame/blob/master/MonoGame.Framework/Android/Devices/Sensors/Accelerometer.cs 
        //                         * // Odpocitavanie gravitacie 
        //                        */
        //                        bufferArrayAccel = StringToByte((DateTime.Now.Ticks / 1000) + ";" + (((MotionVector)a.Value).X + ";" + ((MotionVector)a.Value).Y + ";" + ((MotionVector)a.Value).Z));
        //                        break;

        //                    case MotionSensorType.Compass:
        //                        bufferArrayComp = StringToByte(";" + a.Value.Value + ";");
        //                        break;

        //                    case MotionSensorType.Magnetometer:
        //                        bufferArrayMagne = StringToByte((((MotionVector)a.Value).X + ";" + ((MotionVector)a.Value).Y + ";" + ((MotionVector)a.Value).Z + ";"));
        //                        break;

        //                    case MotionSensorType.Gyroscope:
        //                        bufferArrayGyro = StringToByte((((MotionVector)a.Value).X + ";" + ((MotionVector)a.Value).Y + ";" + ((MotionVector)a.Value).Z + "\n"));
        //                        break;
        //                }

        //                lastLog = Environment.TickCount;
        //                WriteBuffersIntoFile(bufferArrayAccel, bufferArrayComp, bufferArrayGyro, bufferArrayMagne, Registration.File);
        //            }
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine("ERR> " + ex);
        //    }
        //}


        // TODO check memory usage
        // TODO init zero values variables
        // TODO check Garbbage collector on Android java
        /// <summary>
        /// End of catching sensors information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public async void OnEndClick(object sender, EventArgs e)
        {   
            btnStop.IsEnabled = false;
            StackLayoutMain.BackgroundColor = Color.Black;
            if(s==null)
            {
                return;
            }
            s.Stop();
            //StackLayoutMain.GestureRecognizers.Remove(tapGestureRecognizer);
            //labelCotrol.TextColor = Color.DarkGreen;
            //labelCotrol.Text = "Not run";

            // Send to server and main.login page
            if (countOfTtest <= 0)
            {   
                if(CrossConnectivity.Current.IsConnected)
                {
                    // TO DO multiple sending or only one
                    foreach (var item in list_of_files)
                    {
                        Debug.WriteLine(item.Name);
                        await SendDataToServerAsync(item);
                    }
                    //var sensorsList = DependencyService.Get<MySensors>().GetSensorsList();
                    //Debug.WriteLine(sensorsList);
                    await SendDataToServerAsync(file);
                }
                // END OF TEST'S
                var rootPage = Navigation.NavigationStack.FirstOrDefault();
                if (rootPage != null)
                {
                    App.IsUserLoggedIn = true;
                    Navigation.InsertPageBefore(new MainPage(), Navigation.NavigationStack.First());
                    await Navigation.PopToRootAsync();
                }
                return;
            }

            // Offline
            //if(true) {
            if (CrossConnectivity.Current.IsConnected)
            {
                await SendDataToServerAsync(file);
                // add to array of files
                list_of_files.Add(file);
                countOfTtest -= 1;
                textTests.Text = (countOfTtest+1).ToString();
            }
            else
            {
                Debug.WriteLine("No Internet is available ");
            }
            btnStart.IsEnabled = false;
            btnStart.IsEnabled = true;
        }

        /// <summary>
        /// Method to convert open IFile to byte array
        /// </summary>
        /// <param name="file">Open IFile</param>
        /// <returns>Byte array converted from IFile</returns>
        public async Task<byte[]> GetBytesAsync(IFile _file)
        {
            byte[] buffer = new byte[100];
            System.IO.Stream stream = null;
            try
            {
                using (stream = await _file.OpenAsync(FileAccess.ReadAndWrite))
                {
                    stream.Write(buffer, 0, 100);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERR GetBytesAsync(): " + ex);
                return null;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
            return buffer;
        }


        /// <summary>
        /// Upload open csv file
        /// </summary>
        /// <returns></returns>
        public async Task SendDataToServerAsync(IFile file)
        {
            var RestUrl = baseUrl + "/mslovik/upload.php";
            var uri = new Uri(string.Format(RestUrl, string.Empty));


            if (file == null)
            {
                Debug.WriteLine("Error: Empty file");
                return;
            }
            //byte[] buffer = await GetBytesAsync(file);
            // Read content from file
            var x = await file.ReadAllTextAsync();

            HttpClient httpClient = null;
            try
            {
                using (httpClient = new HttpClient())
                {
                    var surveyBytes = Encoding.UTF8.GetBytes("T;A_X;A_Y;A_Z;C_V;M_X;M_Y;M_Z;G_X;G_Y;G_Z;\n" + x);

                    // Add headers for upload 
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var byteArrayContent = new ByteArrayContent(surveyBytes);
                    byteArrayContent.Headers.ContentType = MediaTypeHeaderValue.Parse("text/csv");

                    // Http Post with data file
                    var response = await httpClient.PostAsync(uri, new MultipartFormDataContent
                    {
                        {byteArrayContent, "\"file\"", file.Name}
                    });

                    if (response.IsSuccessStatusCode)
                    {
                        Debug.WriteLine(response.StatusCode);
                    }
                    else
                    {
                        // Unsuccefull
                        Debug.WriteLine("Error while uploading file on web server" + response);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            finally
            {
                if (httpClient != null)
                {
                    httpClient.Dispose();
                }
            }
        }// end SendDataToServerAsync()
    }
}
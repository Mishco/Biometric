﻿using System;
using System.Linq;
using Xamarin.Forms;

namespace Biometric
{
	public partial class SignUpPage : ContentPage
	{
        int countOfTest = 21;
		public SignUpPage ()
		{
			InitializeComponent();
		}

		async void OnSignUpButtonClicked (object sender, EventArgs e)
		{
			var user = new User () {
				//Username = usernameEntry.Text,
				Password = passwordEntry.Text,
				Email = emailEntry.Text
			};

            // Sign up logic goes here

            //var firstPage = Navigation.NavigationStack.FirstOrDefault();
            //if (firstPage != null)
            //{
            //    Navigation.InsertPageBefore(new Views.PaintPage(), Navigation.NavigationStack.First());
            //    await Navigation.PopToRootAsync();
            //}

            // Go to registration page
            var signUpSucceeded = AreDetailsValid(user);
            if (signUpSucceeded)
            {
                var paintPage = Navigation.NavigationStack.FirstOrDefault();
                if(paintPage != null)
                {
                    Navigation.InsertPageBefore(new Views.PaintPage(user, this.countOfTest), Navigation.NavigationStack.First());
                    await Navigation.PopToRootAsync();
                }

                //var registrationPage = Navigation.NavigationStack.FirstOrDefault();
                //if (registrationPage != null)
                //{
                //    Navigation.InsertPageBefore(new Registration(user, this.countOfTest), Navigation.NavigationStack.First());
                //    await Navigation.PopToRootAsync();
                //}
            }
            else
            {
                messageLabel.Text = "Sign up failed!";
            }
            //var signUpSucceeded = AreDetailsValid (user);
            //if (signUpSucceeded) {
            //	var rootPage = Navigation.NavigationStack.FirstOrDefault ();
            //	if (rootPage != null) {
            //		App.IsUserLoggedIn = true;
            //		Navigation.InsertPageBefore (new MainPage (), Navigation.NavigationStack.First ());
            //		await Navigation.PopToRootAsync ();
            //	}
            //} else {
            //	messageLabel.Text = "Sign up failed";
            //}
        }

		bool AreDetailsValid (User user)
		{
			return (!string.IsNullOrWhiteSpace (user.Password) && !string.IsNullOrWhiteSpace (user.Email) && user.Email.Contains ("@"));
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Rg.Plugins.Popup;
using Rg.Plugins.Popup.Pages;
using System.Diagnostics;

using Rg.Plugins.Popup.Services;



namespace Biometric.Views
{
    
    public partial class TextPage : PopupPage
    {
        public TextPage()
        {
            try
            {
                InitializeComponent();
            }catch(Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private void OnClose(object sender, EventArgs e)
        {
            PopupNavigation.PopAsync();
        }

        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.5);
        }

        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(1);
        }

    }
}
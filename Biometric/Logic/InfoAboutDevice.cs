﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeviceMotion.Plugin;
using Plugin.DeviceInfo;
using System.Diagnostics;

namespace Biometric.Logic
{
    class InfoAboutDevice
    {
        private String version;
        private String platform;
        private String model;
        private String id;
        private String versionNumber;

        public InfoAboutDevice()
        {
            try
            { 
                this.version = "Version: "+ CrossDeviceInfo.Current.Version;
                this.platform = "Platform: " + CrossDeviceInfo.Current.Platform;
                this.model = CrossDeviceInfo.Current.Model;
                this.id = "Id: " + CrossDeviceInfo.Current.Id;
                this.versionNumber = "VersionNumber: " + CrossDeviceInfo.Current.VersionNumber;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR> InfoAboutDevice(): " + ex.Message);
            }
        }

        public void InfoAboutSensors()
        {
        
        }

        public String Version => this.version;
        public String Platform => this.platform;
        public String Model => this.model;
        public String Id => this.id;
        public String VersionNumber => this.versionNumber;
    }
}

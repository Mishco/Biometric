﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PCLStorage;

namespace Biometric.Logic
{
    class CreateDataFile
    {
        private int userId;
        private static IFile _file;

        public CreateDataFile(int UserId)
        {
            this.userId = UserId;
        }

        public async void DoCreateDataFile(int countOfTtest)
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;
            // create a folder, if one does not exist already
            IFolder folder = await rootFolder.CreateFolderAsync("DataFolder", CreationCollisionOption.OpenIfExists);
            String nameOffile = userId + "_" + (DateTime.Now.Ticks / 1000) + "_" + countOfTtest + "_data_" + new InfoAboutDevice().Model + ".csv";

            //create a file, overwriting any existing file
            _file = await folder.CreateFileAsync(nameOffile, CreationCollisionOption.ReplaceExisting);
        }

        public static IFile _File
        {
            get
            {
                if(_file == null)
                {
                    return null;
                }
                return _file;
            }

        }
        

    }
}

﻿using DeviceMotion.Plugin;
using System;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;

using PCLStorage;
using DeviceMotion.Plugin.Abstractions;
using Xamarin.Forms;

namespace Biometric.Logic
{
    class SensorsCatching
    {

        // For every sensor individually byte buffer 
        byte[] bufferArrayAccel = null;
        byte[] bufferArrayComp = null;
        byte[] bufferArrayGyro = null;
        byte[] bufferArrayMagne = null;
        bool acc = false, com = false, mag = false, gyr = false;

        private static readonly object padlock = new object();

        public async void Stop()
        {
            CrossDeviceMotion.Current.SensorValueChanged -= SensorsMethod();

            CrossDeviceMotion.Current.Stop(MotionSensorType.Accelerometer);
            CrossDeviceMotion.Current.Stop(MotionSensorType.Compass);
            CrossDeviceMotion.Current.Stop(MotionSensorType.Gyroscope);
            CrossDeviceMotion.Current.Stop(MotionSensorType.Magnetometer);
            bufferArrayAccel = null;
            bufferArrayComp = null;
            bufferArrayGyro = null;
            bufferArrayMagne = null;
        }
        public async void Run()
        {
            

            lock (padlock)
            {
                // Call MainActiviyt in Biometric.Droid
                var text = DependencyService.Get<MySensors>().GetToast();
                Debug.WriteLine(text);

                CrossDeviceMotion.Current.Start(MotionSensorType.Accelerometer, MotionSensorDelay.Fastest);
                CrossDeviceMotion.Current.Start(MotionSensorType.Compass, MotionSensorDelay.Fastest);
                CrossDeviceMotion.Current.Start(MotionSensorType.Gyroscope, MotionSensorDelay.Fastest);
                CrossDeviceMotion.Current.Start(MotionSensorType.Magnetometer, MotionSensorDelay.Fastest);
            }

            bufferArrayAccel = Encoding.UTF8.GetBytes("");
            bufferArrayComp = Encoding.UTF8.GetBytes("");
            bufferArrayGyro = Encoding.UTF8.GetBytes("");
            bufferArrayMagne = Encoding.UTF8.GetBytes("");

            // If sensors catch same value write them
            CrossDeviceMotion.Current.SensorValueChanged += SensorsMethod();
        }

        private SensorValueChangedEventHandler SensorsMethod()
        {
            
            return async (s, a) =>
            {
                switch (a.SensorType)
                {
                    case MotionSensorType.Accelerometer:
                        acc = true;
                        bufferArrayAccel = StringToByte((DateTime.Now.Ticks / 1000) + ";" + (((MotionVector)a.Value).X + ";" + ((MotionVector)a.Value).Y + ";" + ((MotionVector)a.Value).Z));
                        break;
                    case MotionSensorType.Compass:
                        com = true;
                        bufferArrayComp = StringToByte(";" + a.Value.Value + ";");
                        break;
                    case MotionSensorType.Magnetometer:
                        mag = true;
                        bufferArrayMagne = StringToByte((((MotionVector)a.Value).X + ";" + ((MotionVector)a.Value).Y + ";" + ((MotionVector)a.Value).Z + ";"));
                        break;
                    case MotionSensorType.Gyroscope:
                        gyr = true;
                        bufferArrayGyro = StringToByte((((MotionVector)a.Value).X + ";" + ((MotionVector)a.Value).Y + ";" + ((MotionVector)a.Value).Z + ";"));
                        break;
                }
                if (acc && com && mag)
                {
                    acc = com = mag = gyr = false;
                    WriteBuffersIntoFile(bufferArrayAccel, bufferArrayComp, bufferArrayGyro, bufferArrayMagne, Registration.File);
                }
            };
        }

        byte[] StringToByte(String text)
        {
            return Encoding.UTF8.GetBytes(text);
        }

        async void WriteBuffersIntoFile(byte[] buffAcc, byte[] buffComp, byte[] buffGyro, byte[] buffMagn, IFile file)
        {
            
            byte[] bytes = Encoding.UTF8.GetBytes("");
            byte[] endOfLine = Encoding.UTF8.GetBytes("\n");
            if (file != null)
            {
                if (buffAcc != null && buffComp != null && buffGyro != null && buffMagn != null)
                {
                        bytes = buffAcc.Concat(buffComp).Concat(buffMagn).Concat(buffGyro).Concat(endOfLine).ToArray();
                        Stream streamToWrite = null;
                        try
                        {
                            using (streamToWrite = await file.OpenAsync(FileAccess.ReadAndWrite).ConfigureAwait(false))
                            {
                                streamToWrite.Position = streamToWrite.Length;
                                
                                if (streamToWrite.CanWrite)
                                {
                                   await streamToWrite.WriteAsync(bytes, 0, bytes.Length).ConfigureAwait(false);
                                }
                            }
                        }
                        catch (Exception exe)
                        {
                        // System.IO.IOException: Sharing violation on path /data/data/com.mishco.Biometric/files/DataFolder/3_636433899487625_4_data_GT-I9195.csv
                        // Debug.WriteLine("Writing csv: " + exe);
                        }
                        finally
                        {
                            if (streamToWrite != null)
                            {
                                streamToWrite.Dispose();
                            }
                        }
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PCLStorage;
using System.IO;
using System.Diagnostics;

namespace Biometric.Logic
{
    public static class PCLHelper
    {
        /// <summary>
        /// Metoda na zistenie ci existuje subor
        /// </summary>
        /// <param name="fileName">nazos hladaneho suboru</param>
        /// <param name="rootFolder">priecinok v ktorom sa bude hladat</param>
        /// <returns>True ak existuje inak false</returns>
        public async static Task<bool> IsFileExistAsync(this string fileName, IFolder rootFolder = null)
        {
            IFolder folder = rootFolder ?? FileSystem.Current.LocalStorage;
            ExistenceCheckResult folderexist = await folder.CheckExistsAsync(fileName);
            if (folderexist == ExistenceCheckResult.FileExists)
            {
                return true;
            }
            return false;
        }

        public static async Task AppendAllTextAsync(this IFile file, string contents)
        {
            if (file != null)
            {
                try
                {
                    using (var stream = await file.OpenAsync(FileAccess.ReadAndWrite).ConfigureAwait(false))
                    {
                        if(stream.CanWrite) { 
                            stream.Seek(stream.Length, SeekOrigin.Begin);
                            using (var sw = new StreamWriter(stream))
                            {
                                await sw.WriteAsync(contents).ConfigureAwait(false);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Debug.WriteLine("AppendALlText: " + ex);
                }
            }
        }
    }
}
